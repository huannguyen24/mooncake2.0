'use strict';

/**
 * mooncake-products service
 */

const { createCoreService } = require('@strapi/strapi').factories;

module.exports = createCoreService('api::mooncake-products.mooncake-products');
