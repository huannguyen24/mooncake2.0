'use strict';

/**
 * mooncake-products controller
 */

const { createCoreController } = require('@strapi/strapi').factories;

module.exports = createCoreController('api::mooncake-products.mooncake-products');
