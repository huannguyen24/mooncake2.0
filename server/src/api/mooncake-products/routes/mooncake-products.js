'use strict';

/**
 * mooncake-products router
 */

const { createCoreRouter } = require('@strapi/strapi').factories;

module.exports = createCoreRouter('api::mooncake-products.mooncake-products');
