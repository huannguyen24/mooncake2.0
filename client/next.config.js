/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        remotePatterns: [
          {
            protocol: 'https',
            hostname: 'shutters-slider.uiinitiative.com',
            port: '',
            pathname: '/images/*',
          },
        ],
      },
}

module.exports = nextConfig
