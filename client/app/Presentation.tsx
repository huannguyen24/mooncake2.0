import React from 'react'

const Presentation = () => {
  return (
    <section className="section sticky section--s1">
      <h1>Markup</h1>
      <p>The page is laid out using <span className="pre">&#60;header&#62;</span>, <span className="pre">&#60;section&#62;</span>, and <span className="pre">&#60;footer&#62;</span> tags. Any section that will stick to the top of the page, like this one, has a <span className="pre">.sticky</span> className.</p>
    </section>
  )
}

export default Presentation