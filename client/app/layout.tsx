import type { Metadata } from 'next'
import { Playfair } from 'next/font/google'
import './globals.css'


const inter = Playfair({ subsets: ['vietnamese'] })

export const metadata: Metadata = {
  title: 'Thinh Hien Mooncakes',
  description: 'Mooncakes',
  authors: [{
    name: 'Thinh Hien',
  }],
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <head>
        <link
          rel="stylesheet"
          href="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.css"
        />

        <script src="https://cdn.jsdelivr.net/npm/swiper@10/swiper-bundle.min.js" defer></script>
      </head>
      <body className={inter.className}>
        <main className='min-h-[90vh] relative'>
          {children}
        </main>
      </body>
    </html>
  )
}
