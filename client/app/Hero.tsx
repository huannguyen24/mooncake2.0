import Slider from "@/components/Swiper/Slider"

const Hero = () => {
    return (
        <section className="w-full relative block overflow-hidden -order-1 mb-[0.375rem] transition duration-1000">
            <Slider />
        </section >
    )
}

export default Hero