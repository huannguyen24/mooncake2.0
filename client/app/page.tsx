"use client"

import { Suspense } from 'react'
import Hero from './Hero'
import Presentation from './Presentation'
import Products from './Products'
import Contact from './Contact'

import { useInView } from "react-intersection-observer";
import ScrollWrapper from './ScrollWrapper'

export default function Home() {
  const { ref, inView } = useInView({
    threshold: 0.05
  });
  return (
    <>
      <Hero />
      <div>asdasds</div>
      <ScrollWrapper inView={inView}>
        <div ref={ref}>
          <Presentation />
          <Products />
          <Contact />
        </div>
      </ScrollWrapper>
    </>
  )
}
