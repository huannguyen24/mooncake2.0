import React from 'react'

const Products = () => {
  return (
    <section className="section sticky section--s2">
      <h1>Styling</h1>
      <p>Each <span className="pre">.sticky</span> section of the page is styled to cover the full screen height using <span className="pre">max-height:100vh; min-height: 100vh; height: 100vh;</span>. It is important that the section is no taller otherwise the contents will be hidden as.</p>
      <p>The section sticks using <span className="pre">position:-webkit-sticky; position: sticky; top: -1px;</span> where <span className="pre">top</span> sets the point at which the section sticks.</p>
    </section>
  )
}

export default Products