import * as React from "react";
import { CSSProperties } from "react";
import Header from "@/components/Header/Header";

type Props = {
  children: React.ReactNode;
  inView: boolean;
  style?: CSSProperties;
};

/**
 * ScrollWrapper directs the user to scroll the page to reveal it's children.
 * Use this on Modules that have scroll and/or observer triggers.
 */
const ScrollWrapper = ({ children, style, inView, ...props }: Props) => {
  return (
    <>
      <Header inView={inView} />
      {children}
    </>
  );
};

export default ScrollWrapper;
