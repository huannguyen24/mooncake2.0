import React from 'react'

const GroupSection = ({ title, children }: {
    title: string,
    children: React.ReactNode
}) => {
    return (
        <section className='flex flex-row'>
            <div className='w-[30vw]'>
                <h4 className='text-3xl'>{title}</h4>
            </div>
            <div className='w-auto'>
                {children}
            </div>
        </section>
    )
}

export default GroupSection