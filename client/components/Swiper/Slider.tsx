'use client'
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

import { Navigation, Pagination, Scrollbar, A11y, Parallax } from 'swiper/modules';
import 'swiper/css';
import 'swiper/css/pagination';
import 'swiper/css/navigation';

import "./style.scss"
import Image from 'next/image';


const Slider = () => {

    return (
        <Swiper
            speed={1200}
            parallax={true}
            pagination={{
                clickable: true,
            }}
            navigation={true}
            modules={[Parallax, Pagination, Navigation]}
        >
            <SwiperSlide className='overflow-hidden h-screen w-screen'>
                <Image data-swiper-parallax="50%" width={screen.width} height={screen.height} src={'https://shutters-slider.uiinitiative.com/images/2.jpg'} alt='' className='swiper-shutters-image' />
                <div className='font-bold text-[58px] absolute left-[10%] bottom-[10%] right-[10%] z-20' data-swiper-parallax="50%">Michele</div>
            </SwiperSlide>
            <SwiperSlide className='overflow-hidden'>
                <Image data-swiper-parallax="50%" width={screen.width} height={screen.height} src={'https://shutters-slider.uiinitiative.com/images/3.jpg'} alt='' className='swiper-shutters-image' />
                <div className='swiper-shutters-image-clones'>
                    <div className='swiper-shutters-image-clone left-0' data-swiper-parallax="10%">
                        <Image data-swiper-parallax="10%" width={screen.width} height={screen.height} src={'https://shutters-slider.uiinitiative.com/images/3.jpg'} alt='' className='swiper-shutters-image left-0' />
                    </div>
                    <div className='swiper-shutters-image-clone left-[20%]' data-swiper-parallax="-40%">
                        <Image data-swiper-parallax="10%" width={screen.width} height={screen.height} src={'https://shutters-slider.uiinitiative.com/images/3.jpg'} alt='' className='swiper-shutters-image left-[-100%]' />
                    </div>
                    <div className='swiper-shutters-image-clone left-[40%]' data-swiper-parallax="30%">
                        <Image data-swiper-parallax="10%" width={screen.width} height={screen.height} src={'https://shutters-slider.uiinitiative.com/images/3.jpg'} alt='' className='swiper-shutters-image left-[-200%]' />
                    </div>
                    <div className='swiper-shutters-image-clone left-[60%]' data-swiper-parallax="-80%">
                        <Image data-swiper-parallax="10%" width={screen.width} height={screen.height} src={'https://shutters-slider.uiinitiative.com/images/3.jpg'} alt='' className='swiper-shutters-image left-[-300%]' />
                    </div>
                    <div className='swiper-shutters-image-clone left-[80%]' data-swiper-parallax="50%">
                        <Image data-swiper-parallax="10%" width={screen.width} height={screen.height} src={'https://shutters-slider.uiinitiative.com/images/3.jpg'} alt='' className='swiper-shutters-image left-[-400%]' />
                    </div>
                </div>
                <div className='font-bold text-[58px] absolute left-[10%] bottom-[10%] right-[10%] z-20' data-swiper-parallax="50%">Michele</div>

            </SwiperSlide>
            <SwiperSlide className='overflow-hidden'>
                <Image data-swiper-parallax="50%" width={screen.width} height={screen.height} src={'https://shutters-slider.uiinitiative.com/images/1.jpg'} alt='' className='swiper-shutters-image' />
                <div className="swiper-shutters-image-clones">
                    <div className="swiper-shutters-image-clone " data-swiper-parallax="10%" style={{
                        width: '20%', left: '0%'
                    }}>
                        <Image width={screen.width} height={screen.height} className="swiper-shutters-image left-[0%]" src={'https://shutters-slider.uiinitiative.com/images/1.jpg'} alt="" data-swiper-parallax="10%" />
                    </div>
                    <div className="swiper-shutters-image-clone" data-swiper-parallax="-40%" style={{
                        width: '20%', left: '20%',
                        transform: 'translate3d(-40%, 0px, 0px)'
                    }}>
                        <Image width={screen.width} height={screen.height} className="swiper-shutters-image left-[-100%]" src={'https://shutters-slider.uiinitiative.com/images/1.jpg'}
                            alt="" data-swiper-parallax="10%" style={{
                                transform: 'translate3d(10%, 0px, 0px)'
                            }} />
                    </div>
                    <div className="swiper-shutters-image-clone" data-swiper-parallax="30%" style={{
                        width: '20%', left: '40%',
                        transform: 'translate3d(30%, 0px, 0px)'
                    }}>
                        <Image width={screen.width} height={screen.height} className="swiper-shutters-image left-[-200%]" src={'https://shutters-slider.uiinitiative.com/images/1.jpg'}
                            alt="" data-swiper-parallax="10%" style={{
                                transform: 'translate3d(10%, 0px, 0px)'
                            }} /></div>
                    <div className="swiper-shutters-image-clone" data-swiper-parallax="-80%" style={{
                        width: '20%', left: '60%',
                        transform: 'translate3d(-80%, 0px, 0px)'
                    }}>
                        <Image width={screen.width} height={screen.height} className="swiper-shutters-image left-[-300%]" src={'https://shutters-slider.uiinitiative.com/images/1.jpg'} alt="" data-swiper-parallax="10%" style={{
                            transform: 'translate3d(10%, 0px, 0px)'
                        }} />
                    </div>
                    <div className="swiper-shutters-image-clone" data-swiper-parallax="50%" style={{
                        width: '20%', left: '80%',
                        transform: 'translate3d(50%, 0px, 0px)'
                    }}>
                        <Image width={screen.width} height={screen.height} className="swiper-shutters-image left-[-400%]" src={'https://shutters-slider.uiinitiative.com/images/1.jpg'}
                            alt="" data-swiper-parallax="10%" style={{
                                transform: 'translate3d(10%, 0px, 0px)'
                            }} />
                    </div>
                </div>
                <div className='font-bold text-[58px] absolute left-[10%] bottom-[10%] right-[10%] z-20' data-swiper-parallax="50%">Michele</div>
            </SwiperSlide>
        </Swiper>
    )
}

export default Slider