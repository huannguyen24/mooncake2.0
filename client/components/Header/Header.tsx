


import Image from "next/image"
import Link from "next/link"
import { Bars3BottomRightIcon } from '@heroicons/react/24/solid'
import { useInView } from "react-intersection-observer"


type Props = { inView: boolean };

const Header = ({ inView }: Props) => {
    return (
        <header className={`lg:max-w-7xl m-auto p-5 ${!inView ? 'fixed top-0 z-10 left-0 right-0' : 'hidden'} flex flex-row items-center justify-between`}>
            <Link href={"/"}>
                <Image src="/White_logo.svg" alt="Next.js Logo" width={180} height={37} priority />
            </Link>
            <ul className="hidden lg:flex flex-row gap-5 text-white header-menu">
                <li className="text-2xl min-w-[120px] m-auto">
                    <Link href={"/products"} className="custom-menuLink">
                        Giới thiệu</Link>
                </li>
                <li className="text-2xl min-w-[120px] m-auto">
                    <Link href={"/products"} className="custom-menuLink">
                        Sản phẩm</Link>
                </li>
                <li className="text-2xl min-w-[120px] m-auto">
                    <Link href={"/products"} className="custom-menuLink  border-black">
                        Liên hệ</Link>
                </li>

            </ul>
            <div className="hidden lg:block">
                <button className="text-3xl text-white uppercase py-2 px-6 rounded hover:bg-[#2b160f]  transition-all duration-200">Mua ngay</button>
            </div>
            <div className="block lg:hidden">
                <Bars3BottomRightIcon className="w-10 h-10 md:h-20 md:w-20 text-white" />
            </div>
        </header >
    )
}

export default Header